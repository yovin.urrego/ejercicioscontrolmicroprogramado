
-- VHDL Instantiation Created from source file Ejercicio2.vhd -- 22:04:34 07/28/2022
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT Ejercicio2
	PORT(
		clk : IN std_logic;
		inSerie : IN std_logic;
		modoC : IN std_logic;          
		q : OUT std_logic_vector(3 downto 0)
		);
	END COMPONENT;

	Inst_Ejercicio2: Ejercicio2 PORT MAP(
		q => ,
		clk => ,
		inSerie => ,
		modoC => 
	);


