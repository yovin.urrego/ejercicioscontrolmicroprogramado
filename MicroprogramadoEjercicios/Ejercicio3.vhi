
-- VHDL Instantiation Created from source file Ejercicio3.vhd -- 22:01:26 07/28/2022
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT Ejercicio3
	PORT(
		clk : IN std_logic;
		P : IN std_logic;
		T : IN std_logic;
		Q : IN std_logic;          
		salida : OUT std_logic_vector(1 downto 0)
		);
	END COMPONENT;

	Inst_Ejercicio3: Ejercicio3 PORT MAP(
		clk => ,
		P => ,
		T => ,
		Q => ,
		salida => 
	);


