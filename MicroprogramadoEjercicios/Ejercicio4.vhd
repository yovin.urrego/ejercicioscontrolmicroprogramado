----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:30:29 07/28/2022 
-- Design Name: 
-- Module Name:    Ejercicio4 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Ejercicio4 is port
(clk,L,R, rst :in std_logic;
s: in std_logic_vector(1 downto 0);
p:in std_logic_vector(2 downto 0);
q: out std_logic_vector(3 downto 0));
end Ejercicio4;

architecture Behavioral of Ejercicio4 is
begin
corrimientos : process (clk,L,R)
begin
 if (rising_edge(clk)) then 
	if (rst = '1')	then
		q<="0000";
	elsif  (s = "00")then 
		 q(3)<='0';
		 q(2)<= p(2);
		 q(1)<=p(1);
		 q(0)<= p(0);
	elsif(s = "01") then 
		 q(3)<=p(2);
		 q(2)<=p(1);
		 q(1)<=p(0);
		 q(0)<=L;
	elsif (s = "10") then
		 q(3)<=R;
		 q(2)<= '0';
		 q(1)<=p(2);
		 q(0)<=p(1);
	elsif(s ="11") then 
		 q(3)<='0';
		 q(2)<= not p(2);
		 q(1)<=not p(1);
		 q(0)<=not p(0);
	end if;
 end if;
 end process corrimientos;

end Behavioral;

