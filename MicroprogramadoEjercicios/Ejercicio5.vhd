----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:50:25 07/29/2022 
-- Design Name: 
-- Module Name:    Ejercicio5 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Ejercicio5 is port(
clk,B,Fx,Fb,Cont4,boton : in std_logic;
  salida : out Std_logic_Vector (3 downto 0));
end Ejercicio5;

architecture Behavioral of Ejercicio5 is
type estados is (Inicio,C,D,E,F,G,H,I,J,incrementa,alarma,incrementa1);
signal presente ,futuro: estados;
begin
proceso1: process(B,Fx,Fb,Cont4,boton) begin
   case presente is 
	  when Inicio =>salida<="0000"; 
	    futuro <= C;
	when C => salida <= "0001";
	  if B= '1' then 
	  futuro <=D;
	  else
	  futuro <=C;
	  end if;
	  when D => salida<="0010";
	  if Fx='1' then
	  futuro <= E;
	  else
	  futuro <= H;
	  end if;
	  when E =>salida <="0011";
	  if Fb = '1' then
	  futuro <= E;
	  else
	  futuro <=F;
	  end if;
	  when F => salida <="0100";
	  if Cont4 ='1' then 
	  futuro <=incrementa;
	  else
	  futuro <=G;
	  end if;
	  when incrementa=>salida<="0101";
	   futuro<= C;
		when G => salida<="0110";
		futuro <= Inicio;
	  when H => salida <="0111";
	    if Fb ='1' then
		  futuro <=I;
		  else
		  futuro<=H;
		 end if;
	  when I => salida<="1000";
	  if Cont4 ='1' then
	  futuro <= alarma;
	  else
	  futuro<= incrementa1;
	  end if;
	  when incrementa1=> salida <="1001";
	      futuro <=J;
	  when J => salida <= "1010";
	  if boton ='1' then
	   futuro <=H;
		else
		futuro <=J;
		end if;
		when alarma => salida<="1011";
		futuro<=Inicio;
		end case;
		end process proceso1;
		
		procesoClk : process(clk) begin	
		if (clk'event and clk = '1') then
			presente <= futuro;
		end if;
	end process procesoClk; 

end Behavioral;