----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:19:57 07/28/2022 
-- Design Name: 
-- Module Name:    Ejercicio2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Ejercicio2 is
    Port ( q : out  STD_LOGIC_VECTOR (0 to 3);
           clk,inSerie, modoC : in  STD_LOGIC);
end Ejercicio2;

architecture Behavioral of Ejercicio2 is
begin
process (clk,inSerie, modoC)
begin
	if (clk'event and clk = '0')  then
			if	(inSerie = '1' and modoC = '0') then
			q<= "1000";
			elsif (inSerie = '0' and modoC = '0') then
			q<= "0000";
			end if;
	end if;
end process;
end Behavioral;

