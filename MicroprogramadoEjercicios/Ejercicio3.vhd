----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:34:25 07/26/2022 
-- Design Name: 
-- Module Name:    Ejercicio3 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Ejercicio3 is
 Port ( clk,  P,T,Q: in  STD_LOGIC;
           salida : out  STD_LOGIC_VECTOR (1 downto 0));
end Ejercicio3;

architecture Behavioral of Ejercicio3 is
type estados is (INICIO,ALTO,X,FUERZA);
signal presente, futuro : estados;
begin
	proceso1 : process(P,T,Q, presente) begin
		case presente is
			when INICIO => salida <= "00";
				if P = '1' then
					futuro <= ALTO;
				else
					futuro <= INICIO;
				end if;
			when ALTO => salida <= "01";
				if T = '1' then 
					futuro <= X;
				else
					futuro <= ALTO;
				end if;
			when X => salida <= "10";
				if Q = '1' then
					futuro <= FUERZA;
				else
					futuro <= X;
				end if;
			when FUERZA => salida <= "11";
				futuro <= INICIO;
			end case;
		end process proceso1;

	procesoClk : process(clk) begin	
		if clk'event and clk = '1' then
			presente <= futuro;
		end if;
	end process procesoClk;

end Behavioral;

