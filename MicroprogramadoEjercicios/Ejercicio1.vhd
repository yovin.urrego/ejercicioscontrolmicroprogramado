----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:51:09 07/29/2022 
-- Design Name: 
-- Module Name:    Ejercicio1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Ejercicio1 is port(
  P: in std_logic_vector(3 downto 0);
  clk, load, enp, rst: in std_logic;
  Q: inout std_logic_vector (3 downto 0));
end Ejercicio1;
architecture Behavioral of Ejercicio1 is
begin
process(clk, rst, load, enp)begin
if(rst ='1') then 
Q <="0000";
elsif (clk'event and clk='1') then 
 if(load ='0' and (enp='1' or enp ='0')) then
 Q <=P;
elsif(load = '1' and enp='0') then
Q<= Q;
elsif(load = '1' and enp='1') then
Q <= Q +1;
end if;
end if;
end process;
end Behavioral;


